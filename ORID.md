# day6-Gillian

## Objective: 
Through today's code review, I find that I am rarely aware of using class inheritance relationships to improve code reusability. Also, there is the occurrence of magic numbers and test cases that do not fit the business logic in my homework, resulting in unclear code logic. 
What's more, the three groups presented the observer mode, command mode and strategy mode respectively. 
I learned refactor that makes my code clenaner and more maintainable.

## Reflective: 
Refactor is very interesting. This is a process of code upgrade.

## Interpretive: 
I think refactor not only makes the code better, but also a great way to make our thinking more open and active. The same result, through refactor we can constantly find new ways to implement it. For example we can use basic ways like the for loop and advanced ways like the stream api to implement traversing lists. This process also deepens my understanding of java.

## Decisional: 
Remeber the Rule of Three in order to reduce duplicate code


